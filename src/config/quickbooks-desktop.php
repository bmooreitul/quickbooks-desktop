<?php

	return [
		'credentials' 					=> [
			'user' 						=> 'quickbooks', 			//USERNAME FOR THE .QWC FILE
			'pass' 						=> 'GENERATE_PASSWORD_HERE', 				//PASSWORD FOR THE .QWC FILE
		],
		'handler' 						=> [
			'deny_concurrent_logins' 	=> false, 					//TOGGLE WEB CONNECTOR TO RUN IN PARALLELL. (false = multiple, true = single)
			'deny_reallyfast_logins' 	=> false, 					//TOGGLE WEB CONNECTOR LOGIN SPEED (false = no limit, true = once/per minute)
		],
		'driver' 						=> [
			'max_log_history' 			=> 1024, 					//THE MAX RECORDS TO KEEP FOR THE LOG
			//'max_queue_history' 		=> 64, 						//THE MAX RECORDS TO KEEP FOR *successfull* TRANSACTIONS
		],
		'soap' => [
			'server' 					=> 'builtin', 				//CAN BE EITHER: `builtin` or `php`
			'options' 					=> [], 						//AN ARRAY OF SOAP OPTIONS (SEE https://www.php.net/manual/en/soapclient.construct.php)
		],
		'debug' 						=> [
			'log_level' 				=> 3, 						//THE LEVEL OF INFORMATION TO LOG (1 = QUICKBOOKS_LOG_NORMAL, 2 = QUICKBOOKS_LOG_VERBOSE, 3 = QUICKBOOKS_LOG_DEBUG)
			//'email' 					=> 'example@domain.com', 	//THE DEFAULT RECIPIENT FOR DEBUG EMAILS
		],

		/*
		|--------------------------------------------------------------------------
		| Continue Processing After An Error (AS OF itul/quickbooks-desktop:1.1.08)
		|--------------------------------------------------------------------------
		|
		| The holy grail of quickbooks desktop connection performance.
		|
		| Toggle the web connector to keep processing on an error.
		| This value is the default. It can still be overridden by
		| 	- a model `quickbooksOnError()` method
		| 	- a controller error hook in `App/Http/Controllers/QuickBooks/QuickBooksController::"ACTIONNAME"Er` 
		| 		- when "ACTIONNAME" ends with "Er". 
		| 		- For example: "CustomerAddEr"
		|
		*/
		'continue_on_error'				=> false,
	];