<?php

/**
 * Quickbooks Connector
 * 
 * Connecting quickbooks desktop is not easy. But this package tries to make it easier. 
 * Everyline of code in this package is commented so just RTFM
 */

//REGISTER THE COMPOSER AUTOLOADER
require_once __DIR__.'/../vendor/autoload.php';

//BOOT A LARAVEL APPLICATION INSTANCE
($app = require_once realpath(__DIR__.'/../bootstrap/app.php'))->make(\Illuminate\Contracts\Http\Kernel::class)->handle(\Illuminate\Http\Request::capture());

//REQUIRE THE QUICKBOOKS FRAMEWORK
require_once realpath(__DIR__.'/../vendor/itul/quickbooks-desktop-server/QuickBooks.php');

//LOAD THE QUICKBOOKS HANDLERS
//require_once __DIR__.'/../vendor/consolibyte/quickbooks/QuickBooks/WebConnector/Handlers.php';
require_once __DIR__.'/../vendor/itul/quickbooks-desktop-server/QuickBooks/WebConnector/Handlers.php';

//BOOTSTRAP THE QUICKBOOKS FRAMEWORK
require_once __DIR__.'/../app/QuickBooks/bootstrap/quickbooks.php';