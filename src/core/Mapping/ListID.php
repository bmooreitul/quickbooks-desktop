<?php
	
	return [
	
		//ENTITY
		'Customer',
		'Employee',
		'OtherName',
		'Vendor',

		//ENTITY ITEMS
		'ItemDiscount',
		'ItemFixedAsset',
		'ItemGroup',
		'ItemInventory',
		'ItemInventoryAssembly',
		'ItemNonInventory',
		'ItemOtherCharge',
		'ItemPayment',
		'ItemSalesTax',
		'ItemSalesTaxGroup',
		'ItemService',
		'ItemSubtotal',
		'PayrollItemWage',
		'PayrollItemNonWage',

		//Customer and Vendor Profile Lists
		'Currency',
		'CustomerMessage',
		'CustomerType',
		'DateDrivenTerms',
		'JobType',
		'PaymentMethod',
		'SalesRep',
		'SalesTaxCode',
		'ShipMethod',
		'StandardTerms',
		'VendorType',

		//Other Lists
		'Class',
		'BillingRate',
		'PriceLevel',
		'Template',
		'ToDo',
		'Vehicle',

		//Account Lists
		'Account', //* Premier Edition and Above ** Contractor, Professsional Services, Accountant flavors of Premier and above

	];