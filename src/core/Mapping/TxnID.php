<?php
	
	return [

		//Accounts Receivable General Journal
		'Charge',
		'JournalEntry',
		'CreditCardRefund',
		'Check',
		'CreditMemo',
		'Invoice',
		'ReceivePayment',

		//Accounts Payable Deposit
		'Bill',
		'SalesReceipt',
		'BillPaymentCheck',
		'BillPaymentCreditCard',
		'ItemReceipt',
		'Time-Tracking',
		'SalesTaxPaymentCheck',
		'TimeTracking',
		'VendorCredit',

		//Credit Card
		'CreditCardCharge',
		'CreditCardCredit',

		//Non-posting
		'Estimate',
		'PurchaseOrder',
		'SalesOrder', //US Premier Edition and above

		//General Journal
		'JournalEntry',
		'Check',

		//Bank and Sales Receipt
		'Check',
		'Deposit',
		'Sales',
		'Receipt',

		//Time-Tracking
		'TimeTracking',

		//OTHER
		'InventoryAdjustment',
		'BuildAssembly', //US Premier Edition and above
		'VehicleMileage',
	];