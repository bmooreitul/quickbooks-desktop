@php 
    echo '<?xml version="1.0" ?>'."\n";
@endphp
		<QBWCXML>
			<AppName>{{env('APP_NAME')}} {{time()}}</AppName>
			<AppID></AppID>
			<AppURL>{{url('quickbooks/connector')}}</AppURL>
			<AppDescription>{{env('APP_NAME')}} {{time()}}</AppDescription>
			<AppSupport>{{url('/')}}</AppSupport>
			<UserName>{{config('quickbooks-desktop.credentials.user', 'quickbooks')}}</UserName>
			<OwnerID>{<?php echo rand(1,9) ?><?php echo rand(1,9) ?>F<?php echo rand(1,9) ?>B<?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?>-<?php echo rand(1,9) ?><?php echo rand(1,9) ?>F<?php echo rand(1,9) ?>-<?php echo rand(1,9) ?>fac-B<?php echo rand(1,9) ?>FF-<?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?>DE<?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?>D<?php echo rand(1,9) ?><?php echo rand(1,9) ?>}</OwnerID>
			<FileID>{<?php echo rand(1,9) ?><?php echo rand(1,9) ?>F<?php echo rand(1,9) ?>B<?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?>-<?php echo rand(1,9) ?><?php echo rand(1,9) ?>F<?php echo rand(1,9) ?>-<?php echo rand(1,9) ?>fac-B<?php echo rand(1,9) ?>FF-<?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?>DE<?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?><?php echo rand(1,9) ?>D<?php echo rand(1,9) ?><?php echo rand(1,9) ?>}</FileID>
			<QBType>QBFS</QBType>
		<Notify>false</Notify>
				<Scheduler>
					<RunEveryNMinutes>10</RunEveryNMinutes>
				</Scheduler>
			<IsReadOnly>false</IsReadOnly>
		</QBWCXML>