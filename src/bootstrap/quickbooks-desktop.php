<?php

	//SET ERROR REPORTING
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_WARNING);
	ini_set('display_errors', 1);

	use \App\Http\Controllers\QuickBooks\QuickBooksController;

	//BUILD THE TRANSACTION MAPS
	$map = [];
	foreach(get_defined_constants() as $k => $const) foreach(['QUICKBOOKS_ADD_', 'QUICKBOOKS_MOD_', 'QUICKBOOKS_QUERY_', 'QUICKBOOKS_IMPORT_', 'QUICKBOOKS_DERIVE_', 'QUICKBOOKS_AUDIT_', 'QUICKBOOKS_DEL_', 'QUICKBOOKS_VOID_'] as $strCheck) if(strpos($k, $strCheck) !== false) $map[constant($k)] = ['_general_request', '_general_response'];

	//BUILD THE TRANSACTION HOOK MAPS
	$hooks = [];
	foreach(get_defined_constants() as $k => $const) if(strpos($k, 'QUICKBOOKS_HANDLERS_HOOK_') !== false) $hooks[constant($k)] = '_general_hook';

	//BUILD THE DATA SOURCE NAME VALUE
	$dsn = 'mysqli://'.env('DB_USERNAME').':'.env('DB_PASSWORD').'@'.env('DB_HOST').'/'.env('DB_DATABASE');

	//SET THE SOAP WSDL FILE PATH
	//$wsdl = __DIR__.'/../../../vendor/itul/quickbooks-desktop-server/QuickBooks/QBWebConnectorSvc.wsdl';
	$wsdl = realpath(app_path().'/../vendor/itul/quickbooks-desktop-server/QuickBooks/QBWebConnectorSvc.wsdl');

	//ROUTE TRANSACTIONS (IT IS STUPID THAT WE HAVE TO MAP TO A PROCEDURAL FUNCTION, BUT THE CONSOLIBYTE PACKAGE DOES NOT WORK WITHOUT DOING IT THIS WAY)
	function _general_request(...$args){ return (new \App\Http\Controllers\QuickBooks\QuickBooksController)->general_request($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8], $args[9]);}
	function _general_response(...$args){ return (new \App\Http\Controllers\QuickBooks\QuickBooksController)->general_response($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8], $args[9]);}
	function _general_hook(...$args){ return (new \App\Http\Controllers\QuickBooks\QuickBooksController)->general_hook($args[0], $args[1], $args[2], $args[3], $args[4], $args[5]);}
	function _general_error(...$args){ return (new \App\Http\Controllers\QuickBooks\QuickBooksController)->general_error($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8]);}

	//CHECK IF THE DATABASE TABLES HAVE BEEN CREATED YET
	if(!QuickBooks_Utilities::initialized($dsn)){
		QuickBooks_Utilities::initialize($dsn); 				// Initialize creates the neccessary database schema for queueing up requests and logging
		QuickBooks_Utilities::createUser($dsn, config('quickbooks-desktop.credentials.user', 'quickbooks'), config('quickbooks-desktop.credentials.pass', 'password')); 	// This creates a username and password which is used by the Web Connector to authenticate
	}

	// Create a new server and tell it to handle the requests
	(new QuickBooks_WebConnector_Server(
		$dsn, 													//THE DATABASE SERVICE NAME STRING
		$map,  													//AN ARRAY OF MAPPED QB CONSTANTS TO THE FUNCTIONS THEY NEED TO RUN
		['*' => '_general_error'],  							//AN ARRAY OF MAPPED ERRORS TO THE FUNCTIONS THEY NEED TO RUN
		$hooks, 												//AN ARRAY OF MAPPED QB HOOKS TO THE FUNCTIONS THEY NEED TO RUN
		config('quickbooks-desktop.debug.log_level', 3), 		//THE LEVEL OF INFORMATION TO LOG (1 = QUICKBOOKS_LOG_NORMAL, 2 = QUICKBOOKS_LOG_VERBOSE, 3 = QUICKBOOKS_LOG_DEBUG)
		config('quickbooks-desktop.soap.server', 'builtin'), 	//THE TYPE OF SOAP SERVER TO USE
		$wsdl, 													//THE PATH TO THE SOAP SERVER WSDL FILE
		config('quickbooks-desktop.soap.options', []),  		//SOAP OPTIONS
		config('quickbooks-desktop.handler', []),  				//HANDLER OPTIONS
		config('quickbooks-desktop.driver', []),  				//DRIVER OPTIONS
		config('quickbooks-desktop.callback', []) 				//CALLBACK OPTIONS
	))->handle(true, true);

	if(!function_exists('qbxml2obj')){
		function qbxml2obj(string $xml){
			return json_decode(json_encode(\simplexml_load_string($xml)));
		}
	}

	if(!function_exists('getQBKeyFromType')){
		function getQBKeyFromType($type){
			$txnIds = require_once realpath(base_path('vendor/itul/quickbooks-desktop/src/core/Mapping/TxnID.php'));
			//$listIds = require_once realpath(base_path('vendor/itul/quickbooks-desktop/src/core/Mapping/ListID.php'));

			if(in_array($type, $txnIds)) return 'TxnID';
			return 'ListID';
		}
	}