<?php

namespace Itul\QuickbooksDesktop\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuickbooksQueueBase extends Model
{
    use HasFactory;

    protected $table = 'quickbooks_queue';
    protected $primaryKey = 'quickbooks_queue_id';
    public $timestamps = false;

    protected $fillable = [
        'qb_username',
        'qb_action',
        'ident',
        'extra',
        'qbxml',
        'priority',
        'qb_status',
        'enqueue_datetime',
    ];

    protected $casts = [
        'enqueue_datetime' => 'datetime',
        'dequeue_datetime' => 'datetime',
    ];

    protected static function booted(){

        static::creating(function($model){
            $model->enqueue_datetime = \Carbon\Carbon::now();            
        });

        static::saving(function($model){
            if(isset($model->extra)) $model->extra = serialize($model->extra);
        });

        static::retrieved(function($model){
            try{
                if(isset($model->extra)) $model->extra = unserialize($model->extra);
            }
            catch(\Throwable $e){
                //SILENT
            }
        });
    }

    public function quickbooksTransaction(){
        return $this->belongsTo(\App\Models\QuickbooksTransaction::class, 'ident', 'id');
    }

    public function scopeQueued($query){
        return $query->where('qb_status', 'q');
    }

    public function scopeWorking($query){
        return $query->where('qb_status', 'i')->where('dequeue_datetime', '>', \Carbon\Carbon::now()->subMinutes(15));
    }

    public function scopeFailed($query){
        return $query->where('qb_status', 'e')->orWhere(function($q){
            $q->where('qb_status', 'i')->where('dequeue_datetime', '<', \Carbon\Carbon::now()->subMinutes(15));
        });
    }

    public function scopeFinished($query){
        return $query->where('qb_status', 's');
    }

    public function scopeOfStatus($query, $status){
        if(!is_array($status)) $status = [$status];
        return $query->whereIn('qb_status', $status);
    }

    public function scopeOfQuery($query){
        return $query->where('qb_action', 'LIKE', '%Query');
    }

    public function scopeOfAdd($query){
        return $query->where('qb_action', 'LIKE', '%Add');
    }

    public function scopeOfMod($query){
        return $query->where('qb_action', 'LIKE', '%Mod');
    }

    public function scopeOfVoid($query){
        return $query->where('qb_action', 'LIKE', '%Void');
    }

    public function scopeOfDel($query){
        return $query->where('qb_action', 'LIKE', '%Del');
    }

    public function scopeOfAction($query, $action){

        //FORCE THE ACTION TO BE AN ARRAY
        if(!is_array($action)) $action = [$action];

        //GET THE FIRST ACTION
        $firstAction = array_shift($action);

        //START THE QUERY
        $query->where('qb_action', 'LIKE', '%'.$firstAction);

        //ADD THE REST OF THE ACTION QUERIES
        if(count($action)) foreach($action as $a) $query->orWhere('qb_action', 'LIKE', '%'.$a);

        //SEND BACK THE QUERY
        return $query;
    }
}
