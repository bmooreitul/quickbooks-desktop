<?php

namespace Itul\QuickbooksDesktop\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuickbooksTransactionBase extends Model
{
	use HasFactory;

	protected $table = 'quickbooks_transactions';

	protected $_responseObject;

	protected $fillable = [
		'quickbooks_queue_id',
		'parentable_type',
		'parentable_id',
		'action',
		'priority',
		'request_data',
		'response_data',
		'extra',
	];

	protected $casts = [
		'request_data' 	=> 'json',
		'response_data' => 'json',
		'extra' 		=> 'array',
	];

	protected $appends = [
		'responseObject',
	];

	public static function booted(){
		
		static::creating(function($model){

			//PREVENT RECORDS FROM QUEUING MULTIPLE TIMES
			if($parent = $model->parent) if($parent->quickbooksQueues()->queued()->count() || $parent->quickbooksQueues()->working()->count()) return false;
		});

		static::created(function($model){

			$parent = $model->parent;
			if(!$parent){
				try{
					$parentClassName    = '\\'.$model->parentable_type;
					if(class_exists($parentClassName)) $parent = new $parentClassName;
				}
				catch(\Throwable $e){
					//SILENT
				}				
			}

			if($parent && $model->action == 'Delete'){
				$q = \App\Models\QuickbooksQueue::create([
					'qb_username'   => config('quickbooks-desktop.credentials.user'),
					'qb_action'     => 'TxnDel',
					'ident'         => $model->id,
					'priority'      => $model->priority,
					'qb_status'     => 'q',
					'extra' 		=> $model->extra,
				]);

				$model->fill(['quickbooks_queue_id' => $q->quickbooks_queue_id])->save();
			}

			elseif($parent && $model->action == 'Void'){
				$q = \App\Models\QuickbooksQueue::create([
					'qb_username'   => config('quickbooks-desktop.credentials.user'),
					'qb_action'     => 'TxnVoid',
					'ident'         => $model->id,
					'priority'      => $model->priority,
					'qb_status'     => 'q',
					'extra' 		=> $model->extra,
				]);

				$model->fill(['quickbooks_queue_id' => $q->quickbooks_queue_id])->save();
			}

			elseif(!$parent && $model->action == 'Raw'){
				$q = \App\Models\QuickbooksQueue::create([
					'qb_username'   => config('quickbooks-desktop.credentials.user'),
					'qb_action'     => 'RawQuery',
					'ident'         => $model->id,
					'priority'      => $model->priority,
					'qb_status'     => 'q',
					'extra' 		=> $model->extra,
				]);

				$model->fill(['quickbooks_queue_id' => $q->quickbooks_queue_id])->save();
			}
			elseif($parent){
				$q = \App\Models\QuickbooksQueue::create([
					'qb_username'   => config('quickbooks-desktop.credentials.user'),
					'qb_action'     => $parent->quickbooksType.$model->action,
					'ident'         => $model->id,
					'priority'      => $model->priority,
					'qb_status'     => 'q',
					'extra' 		=> $model->extra,
				]);

				$model->fill(['quickbooks_queue_id' => $q->quickbooks_queue_id])->save();
			}
		});

		static::retrieved(function($model){
			$model->responseObject;
		});
	}

	public static function createRaw($priority = 0, array $data = [], $extra = null, $callback = null){
		$extra = !is_array($extra) ? [] : $extra;
		$extra['raw'] = $data;
		if(is_callable($callback)) $extra['callback'] = $callback;
		if(isset($extra['callback']) && is_callable($extra['callback'])) $extra['callback'] = closure_dump($extra['callback']);

		return \App\Models\QuickbooksTransaction::create([
			'action' 			=> 'Raw',
			'priority' 			=> $priority,
			'extra' 			=> $extra,
		]);
	}

	public function refresh(){
		parent::refresh();
		$this->responseObject;
		return $this;
	}

	//SCOPED BY QUERY
	public function scopeQueryRq($query){
		return $query->where('action', 'Query');
	}

	//SCOPED BY ADD
	public function scopeAddRq($query){
		return $query->where('action', 'Add');
	}

	//SCOPED BY MOD
	public function scopeModRq($query){
		return $query->where('action', 'Mod');
	}

	//SCOPED BY WRITE
	public function scopeWriteRq($query){
		return $query->whereIn('action', ['Add','Mod']);
	}

	//SCOPED BY QUEUED
	public function scopeQueuedRq($query){
		return $query->join('quickbooks_queue', 'quickbooks_queue.quickbooks_queue_id', '=', 'quickbooks_transactions.quickbooks_queue_id')->where('quickbooks_queue.qb_status', 'q')->select('quickbooks_transactions.*');
	}

	//SCOPED BY PROCESSING
	public function scopeProcessingRq($query){
		return $query->join('quickbooks_queue', 'quickbooks_queue.quickbooks_queue_id', '=', 'quickbooks_transactions.quickbooks_queue_id')->where('quickbooks_queue.qb_status', 'i')->select('quickbooks_transactions.*');
	}

	//SCOPED BY SUCCESS
	public function scopeSuccessRq($query){
		return $query->join('quickbooks_queue', 'quickbooks_queue.quickbooks_queue_id', '=', 'quickbooks_transactions.quickbooks_queue_id')->where('quickbooks_queue.qb_status', 's')->select('quickbooks_transactions.*');
	}

	//SCOPED BY ERROR
	public function scopeErrorRq($query){
		return $query->join('quickbooks_queue', 'quickbooks_queue.quickbooks_queue_id', '=', 'quickbooks_transactions.quickbooks_queue_id')->where('quickbooks_queue.qb_status', 'e')->select('quickbooks_transactions.*');
	}

	//ALIAS OF scopeErrorRq
	public function scopeFailed($query){
        return $query->errorRq();
    }

    //ALIAS OF scopeSuccessRq
    public function scopeFinished($query){
    	return $query->successRq();
    }

    //ALIAS OF scopeQueuedRq
    public function scopeQueued($query){
    	return $query->queuedRq();
    }

    //ALIAS OF scopeProcessingRq
    public function scopeWorking($query){
    	return $query->processingRq();
    }

	public function parent(){
		return $this->morphTo('parentable');
	}

	public function quickbooksQueue(){
		return $this->belongsTo(\App\Models\QuickbooksQueue::class, 'quickbooks_queue_id', 'quickbooks_queue_id');
	}

	public function quickbooksGenerateData($type){
		if($parent = $this->parent){
			if($type == 'Query'){
				//if(isset($this->quickbooksID)) return [$this->quickbooksKey => $this->quickbooksID];
				if(isset($this->{$this->quickbooksLocalKey})) return [$this->quickbooksLocalKey => $this->{$this->quickbooksLocalKey}];
			}
			return $parent->quickbooksGenerateData($type, $this->extra);
		}
		if($type == 'Query' && isset($this->parentable_type)){
			$parentClassName    = '\\'.$this->parentable_type;
			if(class_exists($parentClassName)){
				$parent             = new $parentClassName;
				return $parent->quickbooksGenerateData($type, $this->extra);
			}            
		}
	}

	public function getResponseObjectAttribute(){
		if(isset($this->_responseObject)) return $this->_responseObject;
		if(is_array($this->response_data) && isset($this->response_data['json']) && file_exists($this->response_data['json'])){
			$res = json_decode(file_get_contents($this->response_data['json']));;
			if(is_array($this->extra) && isset($this->extra['formatReport']) && $this->extra['formatReport'] === true) if(is_object($res) && isset($res->data)) if($res->data = collect((array)$res->data))	foreach($res->data as $rowGroupName => $rowGroup) $res->data[$rowGroupName] = collect($rowGroup);
			$this->_responseObject = $res;
			return $res;
		}
		return null;
	}

	public function getResponseXMLAttribute(){
		if(is_array($this->response_data) && isset($this->response_data['xml']) && file_exists($this->response_data['xml'])) return simplexml_load_file($this->response_data['xml']);
		return null; 
	}

	public function formatReportRs(){

		//DEFINE THE TRANSACTION
		$transaction = $this;

		//DEFINE THE RESPONSE OBJECT AS AN ARRAY
		$responseObject = (array)$transaction->responseObject;

		//INIT THE PARSING ARRAYS
		$report 	= [];
		$colNames 	= [];

		//PARSE THE COLUMN NAMES
		foreach($responseObject['ColDesc'] as $colDesc) $colNames[$colDesc->{'@attributes'}->colID] = @$colDesc->ColTitle->{'@attributes'}->value;

		//PARSE THE ROWS
		foreach($responseObject['ReportData']->DataRow as $row){

			//INIT THE ROW DATA ARRAY
			$rowData = [];
			foreach($colNames as $colName) if(!empty($colName)) $rowData[$colName] = null;

			//PARSE THE ROW COLUMNS
			foreach($row->ColData as $col) if($colId = @$col->{'@attributes'}->colID) $rowData[$colNames[$colId]] = $col->{'@attributes'}->value;

			//ADD THE ROW DATA TO THE REPORT IF IT IS NOT EMPTY
			if(!empty($rowData)) $report[$row->RowData->{'@attributes'}->value][] = $rowData;
		}

		$reportInfo = [
			'title' 	=> $responseObject['ReportTitle'],
			'subtitle' 	=> $responseObject['ReportSubtitle'],
			'basis' 	=> $responseObject['ReportBasis'],
			'data' 		=> $report, 
		];

		//DEFINE THE STORAGE PATH FOR QUICKBOOKS
		$quickbooksPath = storage_path().'/quickbooks/';

		//CREATE THE STORAGE PATH IF NEEDED
		if(!file_exists($quickbooksPath)) mkdir($quickbooksPath, 0755, true);

		//DEFINE THE RESPONSE FILE PATHS
		$jsonPath 	= $quickbooksPath.md5(microtime(true).rand(0,9999)).'.json';

		//STORE THE JSON
		file_put_contents($jsonPath, json_encode($reportInfo, JSON_PRETTY_PRINT));

		//UPDATE THE TRANSACTION
		$transaction->update(['response_data' => ['json' => $jsonPath]]);

		//REFRESH THE TRANSACTION
		$transaction->refresh();
	}

	public function wait_for_finish($display = false, $first = true){

		if($display == true && $first == true){
			\Itul\Buffer\Buffer::flush();
			echo "\n<br>Waiting For Transaction #{$this->id} <br>\n";
		}

		$qb_record = $this->quickbooksQueue()->first();

		if($qb_record->exists){
			if($qb_record->qb_status == 'q'){

				if($display && $first == true){
					echo "\n<br>Status: Queued ";
				}

				if($display){
					for($x = 0; $x < 5; $x++){
						echo '.';
						sleep(1);
					}
				}
				else{
					sleep(5);
				}

				return $this->wait_for_finish($display, false);
			}
			elseif($qb_record->qb_status == 'i'){
				if($display){
					if(!isset($this->inProgressMessageSet) || (isset($this->inProgressMessageSet) && $this->inProgressMessageSet !== true)){
						$this->inProgressMessageSet = true;
						echo "\n<br>The transaction is in progress ";
					}
					
				} 

				date_default_timezone_set('America/Los_Angeles');

				if(strtotime($qb_record->dequeue_datetime) < strtotime("-5 minutes")){	
					if($display){
						for($x = 0; $x < 5; $x++){
							echo '.';
							sleep(1);
						}
					}
					else{
						sleep(5);
					}	
					
					if($display){
						echo "\n<br>Status: Timed Out ";
					}
					return $this->refresh();
				}
				else{						

					if($display){
						for($x = 0; $x < 5; $x++){
							echo '.';
							sleep(1);
						}
					}
					else{
						sleep(5);
					}
					return $this->wait_for_finish($display, false);
				}
			}
			elseif($qb_record->qb_status == 'e'){
				if($display){
					for($x = 0; $x < 5; $x++){
						echo '.';
						sleep(1);
					}
				}
				else{
					sleep(5);
				}
				if($display){
					echo "\n<br>Status: Error ";
				}
				return $this->refresh();
			}
			elseif($qb_record->qb_status == 's'){
				if($display){
					for($x = 0; $x < 5; $x++){
						echo '.';
						sleep(1);
					}
				}
				else{
					sleep(5);
				}
				if($display){
					echo "\n<br>Status: Success! ";
				}

				return $this->refresh();
			}
		}
		
		return $this->refresh();
	}
}