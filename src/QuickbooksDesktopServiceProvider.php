<?php

namespace Itul\QuickbooksDesktop;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Artisan;

class QuickbooksDesktopServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 */
	public function boot(){

		//LOAD MIGRATIONS
		$this->loadMigrationsFrom(__DIR__.'/migrations');

		//LOAD ROUTES
		$this->loadRoutesFrom(__DIR__.'/routes.php');

		//MAKE SURE WE ARE RUNNING IN THE CONSOLE
		if($this->app->runningInConsole()) {			
				
			//DEFINE THE PUBLISHABLE FILES
			$this->publishes([
				__DIR__.'/bootstrap/quickbooks-desktop.php'     		=> base_path('bootstrap/quickbooks-desktop.php'),
				__DIR__.'/config/quickbooks-desktop.php'                => config_path('quickbooks-desktop.php'),
				__DIR__.'/Models/publishable/QuickbooksQueue.php'       => app_path('Models/QuickbooksQueue.php'),
				__DIR__.'/Models/publishable/QuickbooksTransaction.php' => app_path('Models/QuickbooksTransaction.php'),
				__DIR__.'/Controllers/QuickBooksController.php' 		=> app_path('Http/Controllers/QuickBooks/QuickBooksController.php')
			], 'quickbooks-desktop-assets');

			//AUTO PUBLISH THE ASSETS
			Artisan::call('vendor:publish', ['--tag' => 'quickbooks-desktop-assets']);

			//GENERATE A NEW PASSWORD IN THE CONFIG FILE IF NEEDED
			file_put_contents(config_path('quickbooks-desktop.php'), str_replace('GENERATE_PASSWORD_HERE', generateRandomString(), file_get_contents(config_path('quickbooks-desktop.php'))));

			//HTTP REQUEST THE WEB CONNECTOR (BUILDS THE NECESSARY DB TABLES)
			(new \GuzzleHttp\Client)->get(url('/quickbooks/connector'));		

			//RUN MIGRATIONS FOR THIS PACKAGE ONLY AS NEEDED
			foreach(glob(__DIR__.'/migrations/*.php') as $migrationFile) Artisan::call("migrate", ['--path' => str_replace(base_path(), '', $migrationFile)]);
		}
	}
}