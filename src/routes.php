<?php

use Illuminate\Support\Facades\Route;

Route::any('/quickbooks/connector', function(){

	//REQUIRE THE QUICKBOOKS FRAMEWORK
	require_once realpath(app_path().'/../vendor/itul/quickbooks-desktop-server/QuickBooks.php');

	//LOAD THE QUICKBOOKS HANDLERS
	require_once realpath(app_path().'/../vendor/itul/quickbooks-desktop-server/QuickBooks/WebConnector/Handlers.php');

	//BOOTSTRAP THE QUICKBOOKS FRAMEWORK
	require_once app_path().'/../bootstrap/quickbooks-desktop.php';
})->name('quickbooks.connector');

Route::get('quickbooks/generate-qwc', function(){

	$filename = uniqid('blade_');
	$filepath = resource_path("views/tmp/$filename.blade.php");
	if (!file_exists(resource_path("views/tmp"))) mkdir(resource_path("views/tmp"), 0777, true);
	$template = file_get_contents(base_path('vendor/itul/quickbooks-desktop/src/qwctemplate.blade.php'));
	file_put_contents($filepath, trim($template));

	$string = (\Illuminate\Support\Facades\View::make('tmp.'.$filename))->render();

	//BUILD THE FILE
	$fileNamed  = strtolower(str_replace(' ', '-', env('APP_NAME').'-'.env('APP_ENV'))).'.qwc';
	$tempFile   = sys_get_temp_dir().'/'.md5(microtime(true).rand(0,999));
	file_put_contents($tempFile, $string);

	//DOWNLOAD THE FILE
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Disposition: attachment; filename=\"".$fileNamed.'"');
	header("Content-Description: File Transfer");
	@readfile($tempFile);

	//DELETE THE FILE
	unlink($tempFile);
	unlink($filepath);
	exit;
	
})->name('quickbooks.qwc');