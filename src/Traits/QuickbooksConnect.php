<?php

	namespace Itul\QuickbooksDesktop\Traits;

	trait QuickbooksConnect {

		protected $qbAttributes = [];

		//SEND A DEBUG EMAIL
		public function debug_email($content, string $subject = 'Quickbooks Connection Debug', $to = null){

			//GET THE RECIPIENT EMAIL ADDRESS
			if($to = config('quickbooks-desktop.debug.email', $to)){

				//PARSE THE CONTENT AS A STRING
				$content = pr($content, 1);			

				//SET THE EMAIL HEADERS
				$headers = "";

				//CHECK FOR HTML EMAIL
				if(strlen(strip_tags($content)) !== strlen($content)){

					//SET THE HTML EMAIL HEADERS
					$headers = "MIME-Version: 1.0" . "\r\n"; 
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
				}

				@mail($to, $subject, $content, $headers);
			}
		}

		public function initializeQuickbooksConnect(){
			
			if(!property_exists($this, 'quickbooksKey') && property_exists($this, 'quickbooksType')){
				$txnIds = include realpath(base_path('vendor/itul/quickbooks-desktop/src/core/Mapping/TxnID.php'));
				//$listIds = require_once realpath(base_path('vendor/itul/quickbooks-desktop/src/core/Mapping/ListID.php'));
				$this->qbAttributes['quickbooksKey'] = in_array($this->quickbooksType, $txnIds) ? 'TxnID' : 'ListID';
			}
			if((!property_exists($this, 'quickbooksLocalKey') || (property_exists($this, 'quickbooksLocalKey') && is_null($this->quickbooksLocalKey))) && isset($this->quickbooksKey)) $this->quickbooksLocalKey = $this->quickbooksKey;
			if(!property_exists($this, 'quickbooksLocalSequence') || (property_exists($this, 'quickbooksLocalSequence') && is_null($this->quickbooksLocalSequence))) $this->quickbooksLocalSequence = 'EditSequence';

			$this->qbAttributes = [
				'quickbooksType' 			=> $this->quickbooksType,
				'quickbooksKey' 			=> $this->quickbooksKey,
				'quickbooksLocalKey' 		=> $this->quickbooksLocalKey,
				'quickbooksLocalSequence' 	=> $this->quickbooksLocalSequence,
			];

			foreach(array_keys($this->qbAttributes) as $qbAttributeName) if(!in_array($qbAttributeName, $this->hidden)) $this->hidden[] = $qbAttributeName;
			if(!in_array('qbAttributes', $this->hidden)) $this->hidden[] = 'qbAttributes'; 
		}

		//DO SOMETHING WHEN THE MODEL IS BOOTED
		public static function bootQuickbooksConnect(){
			
			
			static::saving(function($model){
				$model->initializeQuickbooksConnect();
				foreach($model->qbAttributes as $k => $v) unset($model->{$k});
			});

			static::saved(function($model){
				if(isset($model->qbAttributes) && !empty($model->qbAttributes)) foreach($model->qbAttributes as $k => $v) $model->$k = $v;
			});
			
			
			//DO SOMETHING HERE
			static::retrieved(function($model){
				$model->initializeQuickbooksConnect();
			});
	    }

	    public function saveQuietly(array $options = []){
	    	$this->initializeQuickbooksConnect();
	    	foreach($this->qbAttributes as $k => $v) unset($this->{$k});
	    	$res = parent::saveQuietly($options);
	    	if(isset($this->qbAttributes) && !empty($this->qbAttributes)) foreach($this->qbAttributes as $k => $v) $this->{$k} = $v;
	    	return $res;
	    }

		//FORCE MODELS THAT USE THIS TRAIT TO HAVE THE GENERATE DATA METHOD
		abstract public function quickbooksGenerateData($type);

		public function save(array $options = []){
			$this->initializeQuickbooksConnect();
			foreach($this->qbAttributes as $k => $v) unset($this->{$k});
			$res = parent::save($options);
			if(isset($this->qbAttributes) && !empty($this->qbAttributes)) foreach($this->qbAttributes as $k => $v) $this->{$k} = $v;
	    	return $res;
		}

		//DEFINE RELATIONSHIP TO TRANSACTIONS
		public function quickbooksTransactions(){
			return $this->morphMany(\App\Models\QuickbooksTransaction::class, 'parentable');
		}

		//DEFINE RELATIONSHIP TO TRANSACTIONS
		public function lastestWriteTransaction(){
			return $this->morphOne(\App\Models\QuickbooksTransaction::class, 'parentable')->writeRq()->latest('updated_at');
		}

		public function lastestSuccessWriteTransaction(){
			return $this->lastestWriteTransaction()->finished();
		}

		public function latestQueryTransaction(){
			return $this->morphOne(\App\Models\QuickbooksTransaction::class, 'parentable')->whereIn('quickbooks_transactions.action', ['Query'])->latest('updated_at');
		}

		public function lastestErrorWriteTransaction(){
			return $this->lastestWriteTransaction()->errorRq();
		}

		public function quickbooksQueues(){
			return $this->belongsToMany(\App\Models\QuickbooksQueue::class, 'quickbooks_transactions', 'parentable_id', 'quickbooks_queue_id')->where('quickbooks_transactions.parentable_type', static::class);
		}

		//RESOLVE A 'TxnID/ListID' FROM A DATABASE FIELD FOR THIS MODEL
		public function getQuickbooksIDAttribute(){
			return $this->attributes[$this->quickbooksLocalKey];
		}

		//SET THE RESOLVED quickbooksID DATABASE FIELD FOR THIS MODEL
		public function setQuickbooksIDAttribute($value){
			$this->attributes[$this->quickbooksLocalKey] = $value;
		}

		//RESOLVE A RETRIEVED 'EditSequence' FROM A DATABASE FIELD FOR THIS MODEL
		public function getQuickbooksSequenceAttribute(){
			return $this->attributes[$this->quickbooksLocalSequence];
		}

		//SET THE RESOLVED 'EditSequence' DATABASE FIELD FOR THIS MODEL
		public function setQuickbooksSequenceAttribute($value){
			$this->attributes[$this->quickbooksLocalSequence] = $value;
		}

		//CHECK IF THIS MODEL HAS ALREADY QUEUED A QUICKBOOKS TRANSACTION
		public function getQuickbooksIsQueuedAttribute(){
			return $this->quickbooksQueues()->queued()->count() ? true : false;
		}

		//PARSE THE QUICKBOOKS REQUEST PARAMETERS
		private function _parseQuickbooksRequestParameters($args){

			//DEFINE THE DEFAULT ARGUMENTS
			$priority 	= 0;
			$extra 		= null;
			$callback 	= null;

			//CHECK IF THE ARGUMENTS WERE PASSED
			if(is_array($args) && !empty($args)){

				//LOOP THROUGH THE ARGUMENTS
				foreach($args as $arg){

					//SET PRIORITY
					if(is_numeric($arg)) $priority 		= $arg;

					//SET EXTRA
					elseif(is_array($arg)) $extra 		= $arg;

					//SET CALLBACK
					elseif(is_callable($arg)) $callback = $arg;
				}
			}

			//CHECK FOR CALLBACK
			if(is_callable($callback)){

				//FORCE THE EXTRA VAR TO BE AN ARRAY
				if(!is_array($extra)) $extra = [];

				//SET THE CALLBACK
				$extra['callback'] = closure_dump($callback);
			}

			//COMPILE THE PARSED VALUES INTO A COMPACTED VERSION
			return compact('priority','extra','callback');
		}

		//TRY TO SEND THIS MODEL TO QUICKBOOKS
		public function quickbooksSend(...$args){

			//PARSE THE ARGUMENTS
			extract($this->_parseQuickbooksRequestParameters($args));

			if($this->exists){

				//CHECK IF THIS MODEL IS ALREADY QUEUED
				if($this->quickbooksIsQueued){
					try{

						//SEND BACK THE TRANSACTION THAT IS CURRENTLY QUEUED
						return $this->quickbooksQueues()->queued()->latest('enqueue_datetime')->first()->quickbooksTransaction;
					}
					catch(\Throwable $e){
						//SILENT
					}
				}

				\DB::beginTransaction();

				//TRY TO CREATE THE TRANSACTION
				try{

					//DEFINE THE ACTION
					$action = isset($this->{$this->quickbooksKey}) ? 'Mod' : 'Add';

					//GENERATE THE DATA IN CASE THERE ARE DEPENCENCIES THAT NEED TO BE SENT BEFORE FINAL DATA GENERATION
					$this->quickbooksGenerateData($action, $extra);

					//CREATE THE TRANSACTION
					$res = \App\Models\QuickbooksTransaction::create([
						'parentable_type' 	=> get_class($this),
						'parentable_id' 	=> $this->id,
						'action' 			=> $action,
						'priority' 			=> $priority,
						'extra' 			=> $extra,
					]);

					//WRITE TO THE DATABASE
					\DB::commit();

					//SEND BACK THE TRANSACTION
					return $res;
				}

				//COULDNT CREATE THE TRANSACTION
				catch(\Throwable $e){

					//UNDO ANY DATABASE CHANGES
					\DB::rollBack();

					//THROW AN ERROR
					throw new \Exception($e->getMessage());
				}
			}

			//THROW AN ERROR
			throw new \Exception("Cannot send a model to quickbooks that does not exist.");
			
		}

		//QUERY RECORDS FROM QUICKBOOKS
		public function quickbooksQuery(...$args){

			//PARSE THE ARGUMENTS
			extract($this->_parseQuickbooksRequestParameters($args));

			//IF THIS MODEL EXISTS READ IT INSTEAD OF USING A QUERY
			if($this->exists) return $this->quickbooksRead($priority, $extra);

			//SEND BACK THE TRANSACTION
			return \App\Models\QuickbooksTransaction::create([
				'parentable_type' 	=> get_class($this),
				'action' 			=> 'Query',
				'priority' 			=> $priority,
				'extra' 			=> $extra,
			]);
		}

		//QUERY A SPECIFIC RECORD IN QUICKBOOKS
		public function quickbooksRead(...$args){

			//PARSE THE ARGUMENTS
			extract($this->_parseQuickbooksRequestParameters($args));

			//SEND BACK THE TRANSACTION
			if($this->exists) return \App\Models\QuickbooksTransaction::create([
				'parentable_type' 	=> get_class($this),
				'parentable_id' 	=> $this->id,
				'action' 			=> 'Query',
				'priority' 			=> $priority,
				'extra' 			=> $extra,
			]);

			//THROW AN ERROR
			throw new \Exception("Cannot read model data from quickbooks before saving it to the database.");
		}

		//VOID A SPECIFIC RECORD IN QUICKBOOKS
		public function quickbooksVoid(...$args){

			//PARSE THE ARGUMENTS
			extract($this->_parseQuickbooksRequestParameters($args));

			//SEND BACK THE TRANSACTION IF THIS MODEL EXISTS
			if($this->exists) return \App\Models\QuickbooksTransaction::create([
				'parentable_type' 	=> get_class($this),
				'parentable_id' 	=> $this->id,
				'action' 			=> 'Void',
				'priority' 			=> $priority,
				'extra' 			=> $extra,
			]);

			//THROW AN ERROR
			throw new \Exception("Cannot void model data in quickbooks before saving it to the database.");
		}

		//DELETE A SPECIFIC RECORD IN QUICKBOOKS
		public function quickbooksDelete(...$args){

			//PARSE THE ARGUMENTS
			extract($this->_parseQuickbooksRequestParameters($args));

			//IF THIS MODEL EXISTS CREATE A NEW TRANSACTION
			if($this->exists) return \App\Models\QuickbooksTransaction::create([
				'parentable_type' 	=> get_class($this),
				'parentable_id' 	=> $this->id,
				'action' 			=> 'Delete',
				'priority' 			=> $priority,
				'extra' 			=> $extra,
			]);

			//THROW AN ERROR
			throw new \Exception("Cannot delete model data in quickbooks before saving it to the database.");
		}
	}