<?php

	namespace Itul\QuickbooksDesktop\Traits;

	trait QuickBooksControllerGeneral {

		//SEND A DEBUG EMAIL
		public function debug_email($content, string $subject = 'Quickbooks Connection Debug', $to = null){

			//GET THE RECIPIENT EMAIL ADDRESS
			if($to = config('quickbooks-desktop.debug.email', $to)){

				//PARSE THE CONTENT AS A STRING
				$content = pr($content, 1);			

				//SET THE EMAIL HEADERS
				$headers = "";

				//CHECK FOR HTML EMAIL
				if(strlen(strip_tags($content)) !== strlen($content)){

					//SET THE HTML EMAIL HEADERS
					$headers = "MIME-Version: 1.0" . "\r\n"; 
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
				}

				@mail($to, $subject, $content, $headers);
			}
		}

		public function general_request($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $version, $locale){

			//DEFINE THE METHOD TO LOOK FOR
			$method_name = $action.'Rq';

			//CHECK IF THE DEFINED METHOD EXISTS
			if(method_exists($this, $method_name)) $xml = $this->$method_name($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $version, $locale);

			//CHECK IF THE TRANSACTION EXISTS
			elseif($transaction = \App\Models\QuickbooksTransaction::find($ID)){

				//GENERATE XML FOR A QUERY
				if($transaction->action == 'Query') $xml = trim((new \Itul\QBXML\QBXML)->convert_to_xml(['QBXMLMsgsRq' => [$method_name => $transaction->quickbooksGenerateData($transaction->action)]]));

				//HANDLE RAW REQUESTS
				elseif($transaction->action == 'Raw'){
					$elementKeys 	= array_keys($extra['raw']);
					$elementKeyName = array_shift($elementKeys);
					$xml 			= trim((new \Itul\QBXML\QBXML)->convert_to_xml(['QBXMLMsgsRq' => [$extra['raw']]]));
					$xml 			= str_replace('<'.$elementKeyName, '<'.$elementKeyName.' requestID="'.$requestID.'" ', $xml);
				}

				//HANDLE DELETE REQUESTS
				elseif($transaction->action == 'Delete'){

					//CHECK IF THE PARENT EXISTS
					if($transactionParent = $transaction->parent){

						//CHECK IF THE PARENT KEY IS TxnID
						if($transactionParent->quickbooksKey == 'TxnID'){

							//GENERATE THE DELETE XML
							$xml = trim((new \Itul\QBXML\QBXML)->convert_to_xml(['QBXMLMsgsRq' => [
								'TxnDelRq' => [
									'TxnDelType' 	=> $transactionParent->quickbooksType,
									'TxnID' 		=> $transactionParent->quickbooksID,
								]						
							]]));
						}

						//CHECK IF THE PARENT KEY IS ListID
						elseif($transactionParent->quickbooksKey == 'ListID'){

							//GENERATE THE DELETE XML
							$xml = trim((new \Itul\QBXML\QBXML)->convert_to_xml(['QBXMLMsgsRq' => [
								'ListDelRq' => [
									'ListDelType' 	=> $transactionParent->quickbooksType,
									'ListID' 		=> $transactionParent->quickbooksID,
								]						
							]]));
						}						
					}					
				}

				//HANDLE VOID REQUESTS
				elseif($transaction->action == 'Void'){

					//CHECK IF THE PARENT EXISTS
					if($transactionParent = $transaction->parent){

						//GENERATE THE VOID XML
						$xml = trim((new \Itul\QBXML\QBXML)->convert_to_xml(['QBXMLMsgsRq' => [
							'TxnVoidRq' => [
								'TxnVoidType' 	=> $transactionParent->quickbooksType,
								'TxnID' 		=> $transactionParent->quickbooksID,
							]						
						]]));
					}
				}

				//CHECK FOR WRITEABLE TRANSACTION REQUESTS
				elseif(in_array($transaction->action, ['Add','Mod'])){

					//GENERATE THE DATA
					$generatedData 	= $transaction->quickbooksGenerateData($transaction->action);

					//REFRESH THE TRANSACTION
					$transaction->refresh();

					//SET THE CURRENT ACTION
					$action 		= $transaction->quickbooksQueue->qb_action;

					//REDEFINE THE METHOD
					$method_name 	= $action.'Rq';

					//GENERATE THE XML
					$xml 			= trim((new \Itul\QBXML\QBXML)->convert_to_xml(['QBXMLMsgsRq' => [$method_name => [$action => $generatedData]]]));
				} 

				//STORE THE XML FOR THIS TRANSACTION
				$transaction->update(['request_data' => ['xml' => $xml]]);
			}

			//TRY TO GENERATE THE XML BECAUSE THE METHOD IS NOT DEFINED
			else $xml = (new \Itul\QBXML\QBXML)->convert_to_xml([$method_name => [$action => $extra]]);

			//SEND BACK THE REQUEST XML
			return trim($xml);
		}

		public function general_response($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $xml, $idents){

			//ASSUME NO ASSOCIATED TRANSACTION
			$transaction = false;

			//TRY TO LOAD THE TRANSACTION
			if($transaction = \App\Models\QuickbooksTransaction::find($ID)){

				if($transaction->action == 'Delete'){
					if($transactionParent = $transaction->parent){
						try{
							$transactionParent->quickbooksID = null;
							$transactionParent->quickbooksSequence = null;
							$transactionParent->save();
						}
						catch(\Throwable $e){
							//SILENT
						}						
					}
				}

				//PARSE THE XML
				$obj = json_decode(json_encode(simplexml_load_string($xml)->QBXMLMsgsRs));

				//LOOP THROUGH THE PARSED XML VALUES
				foreach($obj as $k => $v){

					//LOOP THROUGH THE SUB VALUES OF THE PARSED XML
					foreach($v as $k2 => $v2){

						//CHECK IF THE PROPERTY NAME ENDS IN `Ret`
						if(substr($k2, -3) === 'Ret'){
							$obj = $v2;
							break;
						}
					}
					break;
				}

				//DEFINE THE STORAGE PATH FOR QUICKBOOKS
				$quickbooksPath = storage_path().'/quickbooks/';

				//CREATE THE STORAGE PATH IF NEEDED
				if(!file_exists($quickbooksPath)) mkdir($quickbooksPath, 0755, true);

				//DEFINE THE RESPONSE FILE PATHS
				$jsonPath 	= $quickbooksPath.md5(microtime(true).rand(0,9999)).'.json';
				$xmlPath 	= $quickbooksPath.md5(microtime(true).rand(0,9999)).'.xml';

				//STORE THE RESPONSE FILES
				file_put_contents($jsonPath, json_encode($obj, JSON_PRETTY_PRINT));
				file_put_contents($xmlPath, $xml);
				
				//UPDATE THE TRANSACTION
				$transaction->update(['response_data' => [
					'json' 	=> $jsonPath,
					'xml' 	=> $xmlPath,
				]]);

				$transaction->refresh();

				if(is_array($extra) && isset($extra['formatReport']) && $extra['formatReport'] === true) $transaction->formatReportRs();

				//TRY TO RUN THE CALLBACK
				if(is_array($extra) && isset($extra['callback'])) (new \ClosureWrap($extra['callback']))/*->bind($this->_getTransactionParent($transaction))*/->call($transaction->responseObject, $transaction);
			}

			//DEFINE THE METHOD TO LOOK FOR
			$method_name = $action.'Rs';

			//CHECK IF THE DEFINED METHOD EXISTS
			if(method_exists($this, $method_name)) $this->$method_name($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $xml, $idents);

			//IF THE TRANSACTION WAS FOUND
			if($transaction){

				//TRY TO FIND THE TRANSACTION PARENT
				$transactionParent = $this->_getTransactionParent($transaction);

				//VALIDATE TRANSACTION PARENT AND RESPONSE
				if($transactionParent && $transactionParent->exists && $transaction->responseObject){

					//CHECK IF THE PARENT MODEL NEEDS TO BE UPDATED
					if(is_object($transaction->responseObject) && isset($transaction->responseObject->{$transactionParent->quickbooksLocalKey})){

						if($transaction->action != 'Delete'){

							try{
								//UPDATE THE PARENT MODEL
								$transactionParent->quickbooksID 		= $transaction->responseObject->{$transactionParent->quickbooksLocalKey};
								$transactionParent->quickbooksSequence 	= $transaction->responseObject->EditSequence;
								$transactionParent->save();
							}
							catch(\Throwable $e){
								//SILENT
							}							
						}	
					}

					//TRY TO CALL THE SUCCESS CALLBACK ON THE PARENT
					if(method_exists($transactionParent, 'quickbooksOnSuccess')) $transactionParent->quickbooksOnSuccess($transaction->responseObject, $transaction);
				} 
			}
		}

		
		//HANDLE ERRORS
		public function general_error($requestID, $user, $action, $ID, $extra, $err, $xml, $errnum, $errmsg){

			//DEFAULT CONTINUE RESPONSE TO NULL
			$continue = null;

			//CHECK IF THE TRANSACTION EXISTS
			if($transaction = \App\Models\QuickbooksTransaction::find($ID)){

				//TRY TO FIND THE TRANSACTION PARENT
				$transactionParent = $this->_getTransactionParent($transaction);

				//CHECK FOR EDIT SEQUENCE ERROR
				$this->_editSequenceError($errnum, $transactionParent, $ID);

				//CHECK FOR NAME ALREADY USED ERROR
				$this->_nameAlreadyUsedError($errnum, $transactionParent, $ID);
				
				//CALL THE TRANSACTION PARENT ERROR HOOK IF NEEDED
				if($transactionParent && is_array($transaction->response_data) && isset($transaction->response_data['data']) && method_exists($transactionParent, 'quickbooksOnError')) $continue = $transactionParent->quickbooksOnError($transaction->response_data['data']);
			}
			else{

				//DEFINE THE ERROR CALLBACK NAME
				$method_name = $action.'Er';

				//CALL THE ERROR CALLBACK NAME IF IT EXISTS
				if(method_exists($this, $method_name)) $continue = $this->$method_name($requestID, $user, $action, $ID, $extra, $err, $xml, $errnum, $errmsg);
			}

			//IF NO CONTINUE BOOL VAL HAS BEEN SET CHECK THE CONFIG AND/OR FALLBACK TO FALSE
			if(is_null($continue)) $continue = config('quickbooks-desktop.continue_on_error', false);

			//RETURN THE CONTINUE VALUE
			return $continue;
		}

		/*

		//HANDLE ERRORS
		public function general_error($requestID, $user, $action, $ID, $extra, $err, $xml, $errnum, $errmsg){

			//DEFINE THE ERROR CALLBACK NAME
			$method_name = $action.'Er';

			//CALL THE ERROR CALLBACK NAME IF IT EXISTS
			if(method_exists($this, $method_name)) $this->$method_name($requestID, $user, $action, $ID, $extra, $err, $xml, $errnum, $errmsg);

			//CHECK IF THE TRANSACTION EXISTS
			if($transaction = \App\Models\QuickbooksTransaction::find($ID)){

				//TRY TO FIND THE TRANSACTION PARENT
				$transactionParent = $this->_getTransactionParent($transaction);

				//CHECK FOR EDIT SEQUENCE ERROR
				$this->_editSequenceError($errnum, $transactionParent, $ID);

				//CHECK FOR NAME ALREADY USED ERROR
				$this->_nameAlreadyUsedError($errnum, $transactionParent, $ID);
				
				//CALL THE TRANSACTION PARENT ERROR HOOK IF NEEDED
				if($transactionParent && is_array($transaction->response_data) && isset($transaction->response_data['data']) && method_exists($transactionParent, 'quickbooksOnError')) $transactionParent->quickbooksOnError($transaction->response_data['data']);
			}
		}
		*/

		public function general_hook($requestID, $user, $hook, $err, $hook_data, $callback_config){
			$hookMethodName = str_replace('-', '_', str_replace('QuickBooks_Handlers::', '', $hook)).'_hook';
			if(method_exists($this, $hookMethodName)) $this->$hookMethodName($requestID, $user, $hook, $err, $hook_data, $callback_config);
		}

		private function _getTransactionParent($transaction){

			//TRY TO FIND THE TRANSACTION PARENT
			$transactionParent = $transaction->parent;

			//CHECK IF THE TRANSACTION PARENT WAS NOT FOUND
			if(!$transactionParent && isset($transaction->parentable_type)){

				//DEFINE THE PARENT CLASS NAME
				$parentClassName = '\\'.$transaction->parentable_type;

				//INSTANTIATE THE PARENT CLASS
				if(class_exists($parentClassName)) $transactionParent = new $parentClassName;
			}

			//SEND BACK THE TRANSACTION PARENT
			return $transactionParent;
		}

		private function _nameAlreadyUsedError($errnum, $transactionParent, $ID){

			/*
			if($errnum == 3100 && $transactionParent && $transactionParent->exists){
				$qbType 		= $transactionParent->quickbooksType;
				$qbType 		= $qbType.'Query';
				$generatedData 	= $transactionParent->quickbooksGenerateData('Add');
				$nameKey 		= isset($generatedData['FullName']) ? 'FullName' : 'Name';
				$nameVal 		= $generatedData[$nameKey];

				\App\Models\QuickbooksTransaction::createRaw(900, [
					$qbType.'Rq' 			=> [
						'FullName' 			=> $nameVal,
						'IncludeRetElement' => [
							$transactionParent->quickbooksKey,
							'EditSequence'
						],
					],
				], null, function($res, $transaction) use($ID){

					\DB::beginTransaction();

					try{
						$oldTransaction 			= \App\Models\QuickbooksTransaction::find($ID);
						$parent 					= $oldTransaction->parent;
						$parent->quickbooksID 		= $res->{$parent->quickbooksKey};
						$parent->quickbooksSequence = $res->EditSequence;
						$parent->save();

						//SET THE OLD TRANSACTION REQUEST AND RESPONSE DATA TO EMPTY
						$oldTransaction->request_data 	= null;
						$oldTransaction->response_data 	= null;
						$oldTransaction->action = 'Mod';
						$oldTransaction->save();

						//SET THE OLD TRANSACTION QUEUE TO BE QUEUED AGAIN
						$q 								= $oldTransaction->quickbooksQueue;							
						$q->qb_status 					= 'q';
						$q->dequeue_datetime 			= null;
						$q->qb_action 					= $parent->quickbooksType.'Mod';
						$q->save();

						//STORE TO THE DATABASE
						\DB::commit();

					}
					catch(\Throwable $e){
						\DB::rollBack();
					}
				});
			}
			*/

			//CHECK IF THE EDIT SEQUENCE IS INCORRECT
			if($errnum == 3100 && $transactionParent && $transactionParent->exists) $transactionParent->quickbooksRead(900, [

				//DEFINE THE CALLBACK TO GET THE CURRENT EDITSEQUENCE
				'callback' => closure_dump(function($res, $transaction){

					//SET THE DATABASE ONLY RECORD ON SUCCESS
					\DB::beginTransaction();
					try{

						//GET THE OLD TRANSACTION
						$oldTransaction = \App\Models\QuickbooksTransaction::find($transaction->extra['requeue']);

						//MAKE SURE THE OLD TRANSACTION HASNT ALREADY TRIED TO REQUEUE
						if(is_array($oldTransaction->extra) && isset($oldTransaction->extra['requeued'])) throw new \Exception("This transaction has already been requeued");

						//GET THE EXTTRA ARRAY FROM THE OLD TRANSACTION
						$oldExtra = $oldTransaction->extra;

						//IF THE OLD TRANSACTION DOESNT HAVE AN EXTRA ARRAY SET AN EMPTY ONE
						if(!is_array($oldExtra)) $oldExtra = [];

						//ADD THE REQUESD DATETIME TO THE OLD TRANSACTION EXTRA ARRAY
						$oldExtra['requeued'] = ['transaction_id' => $transaction->id, 'requeued_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')];

						//SET THE OLD TRANSACTION EXTRA VALUE TO USE THE UPDATED ARRAY
						$oldTransaction->extra = $oldExtra;

						//GET THE OLD TRANSACTION PARENT
						$parent = $oldTransaction->parent;
						
						$parent->initializeQuickbooksConnect();

						//SAVE THE OLD TRANSACTION PARENT WITHOUT CALLING ANY LARAVEL HOOKS
						$fillData = [
							$parent->quickbooksLocalKey => $res->{$parent->quickbooksKey},
							$parent->quickbooksLocalSequence => $res->EditSequence,
						];

						\DB::table($parent->getTable())->where('id', $parent->id)->update($fillData);

						$parent->quickbooksID 		= $res->{$parent->quickbooksKey};
						$parent->quickbooksSequence = $res->EditSequence;
						$parent->fresh();

						//SET THE OLD TRANSACTION REQUEST AND RESPONSE DATA TO EMPTY
						$oldTransaction->request_data 	= null;
						$oldTransaction->response_data 	= null;
						$oldTransaction->action 		= 'Mod';
						$oldTransaction->save();

						//SET THE OLD TRANSACTION QUEUE TO BE QUEUED AGAIN
						$q 								= $oldTransaction->quickbooksQueue;							
						$q->qb_status 					= 'q';
						$q->dequeue_datetime 			= null;
						$q->qb_action 					= $parent->quickbooksType.'Mod';
						$q->extra 						= $oldExtra;
						$q->save();

						//STORE TO THE DATABASE
						\DB::commit();
					}

					//SOMETHING WENT WRONG
					catch(\Throwable $e){

						\Log::critical($e->__toString());

						//ROLLBACK ANY CHANGES MADE TO THE DATABASE
						\DB::rollBack();
					}						
				}),

				//SET THE ID OF THE TRANSACTION THAT SHOULD BE REQUEUED
				'requeue' => $ID
			]);
		}

		private function _editSequenceError($errnum, $transactionParent, $ID){

			//CHECK IF THE EDIT SEQUENCE IS INCORRECT
			if($errnum == 3200 && $transactionParent && $transactionParent->exists) $transactionParent->quickbooksRead(900, [

				//DEFINE THE CALLBACK TO GET THE CURRENT EDITSEQUENCE
				'callback' => closure_dump(function($res, $transaction){

					//SET THE DATABASE ONLY RECORD ON SUCCESS
					\DB::beginTransaction();
					try{

						//GET THE OLD TRANSACTION
						$oldTransaction = \App\Models\QuickbooksTransaction::find($transaction->extra['requeue']);

						//MAKE SURE THE OLD TRANSACTION HASNT ALREADY TRIED TO REQUEUE
						if(is_array($oldTransaction->extra) && isset($oldTransaction->extra['requeued'])) throw new \Exception("This transaction has already been requeued");

						//GET THE EXTTRA ARRAY FROM THE OLD TRANSACTION
						$oldExtra = $oldTransaction->extra;

						//IF THE OLD TRANSACTION DOESNT HAVE AN EXTRA ARRAY SET AN EMPTY ONE
						if(!is_array($oldExtra)) $oldExtra = [];

						//ADD THE REQUESD DATETIME TO THE OLD TRANSACTION EXTRA ARRAY
						$oldExtra['requeued'] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

						//SET THE OLD TRANSACTION EXTRA VALUE TO USE THE UPDATED ARRAY
						$oldTransaction->extra = $oldExtra;

						//GET THE OLD TRANSACTION PARENT
						$parent = $oldTransaction->parent;

						//GET THE OLD TRANSACTION PARENT EDITSEQUENCE
						//$parent->quickbooksSequence = $res->EditSequence;

						//SAVE THE OLD TRANSACTION PARENT WITHOUT CALLING ANY LARAVEL HOOKS
						$parent->initializeQuickbooksConnect();

						//SAVE THE OLD TRANSACTION PARENT WITHOUT CALLING ANY LARAVEL HOOKS
						$fillData = [
							$parent->quickbooksLocalKey => $res->{$parent->quickbooksKey},
							$parent->quickbooksLocalSequence => $res->EditSequence,
						];

						\DB::table($parent->getTable())->where('id', $parent->id)->update($fillData);

						$parent->quickbooksID 		= $res->{$parent->quickbooksKey};
						$parent->quickbooksSequence = $res->EditSequence;
						$parent->fresh();

						//SET THE OLD TRANSACTION REQUEST AND RESPONSE DATA TO EMPTY
						$oldTransaction->request_data 	= null;
						$oldTransaction->response_data 	= null;
						$oldTransaction->save();

						//SET THE OLD TRANSACTION QUEUE TO BE QUEUED AGAIN
						$q 								= $oldTransaction->quickbooksQueue;							
						$q->qb_status 					= 'q';
						$q->dequeue_datetime 			= null;
						$q->extra 						= $oldExtra;
						$q->save();

						//STORE TO THE DATABASE
						\DB::commit();
					}

					//SOMETHING WENT WRONG
					catch(\Throwable $e){

						\Log::critical($e->__toString());

						//ROLLBACK ANY CHANGES MADE TO THE DATABASE
						\DB::rollBack();
					}						
				}),

				//SET THE ID OF THE TRANSACTION THAT SHOULD BE REQUEUED
				'requeue' => $ID
			]);
		}
	}