<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quickbooks_transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('quickbooks_queue_id')->nullable();
            $table->nullableMorphs('parentable');
            $table->string('action')->nullable();
            $table->integer('priority')->default(0);
            $table->longText('extra')->nullable();
            $table->binary('request_data')->nullable();
            $table->binary('response_data')->nullable();
            $table->timestamps();
        });

        \DB::statement("ALTER TABLE `quickbooks_transactions` CHANGE `response_data` `response_data` LONGBLOB NULL DEFAULT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quickbooks_transactions');
    }
};
