<?php

	namespace App\Http\Controllers\QuickBooks;


	//-------------------- TRANSACTION HANDLER CLASS -----------------------//
	class QuickBooksController{

		use \Itul\QuickbooksDesktop\Traits\QuickBooksControllerGeneral;

		/*
		public function general_request($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $version, $locale){

			//OVERRIDE THE `general_request` METHOD IN (app/QuickBooks/Traits/QuickBooksControllerGeneral.php)
			//...  DO SOMETHING TO GENERATE XML (INCLUDING ALL/ANY LARAVEL FEATURES/MODELS/HELPERS)
			//return $xml;
		}

		public function general_response($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $xml, $idents){

			//OVERRIDE THE `general_request` METHOD IN (app/QuickBooks/Traits/QuickBooksControllerGeneral.php)
			//...  DO SOMETHING TO GENERATE XML (INCLUDING ALL/ANY LARAVEL FEATURES/MODELS/HELPERS)
			//return $xml;
		}

		public function general_error($requestID, $user, $action, $ID, $extra, $err, $xml, $errnum, $errmsg){

			//OVERRIDE THE `general_error` METHOD IN (app/	QuickBooks/Traits/QuickBooksControllerGeneral.php)
			//... DO SOMETHING WITH AN ERROR HERE
		}
		*/

		/**
		 * EXAMPLE LOGIN SUCCESS HOOK CALLBACK METHOD
		 * 
		 * ALL REQUEST AND RESPONSES DEFAULT TO THEIR GENERIC VERSION (general_request, general_response, general_hook, general_error)
		 * REQUEST METHODS SHOULD BE NAMED AS THE QUICKBOOKS ACTION WITH "Rs" APPENDED (EX: invoiceAdd should be invoiceAddRq)
		 * RESPONSE METHODS SHOULD BE NAMED AS THE QUICKBOOKS ACTION WITH "Rq" APPENDED (EX: invoiceAdd should be invoiceAddRq)
		 * GENERAL REQUESTS AND RESPONSES ARE AUTOMATICALLY CONVERTED FROM AN ARRAY TO QBXML APPROPRIATELY
		 * 
		 * HOOKS CAN BE ONE OF: 
		 * - authenticate_hook
		 * - clientVersion_hook
		 * - closeConnection_hook
		 * - connectionError_hook
		 * - getInteractiveURL_hook
		 * - getLastError_hook
		 * - interactiveDone_hook
		 * - interactiveRejected_hook
		 * - receiveResponseXML_hook
		 * - sendRequestXML_hook
		 * - serverVersion_hook
		 * - login_success_hook
		 * - login_fail_hook
		 * - recurring_hook
		 * - percent_hook
		*/
		
		/*
		public function login_success_hook($requestID, $user, $hook, $err, $hook_data, $callback_config){
			//... DO SOMETHING WITH THE $hook_data VARIABLE HERE
		}
		*/

		/*
		//EXAMPLE INVOICE ADD REQUEST TO OVERRIDE THE GENERIC REQUEST
		public function invoiceAddRq($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $version, $locale){
			//$xml = '...write some XML here';
			//return $xml;
		}

		//EXAMPLE INVOICE ADD RESPONSE TO OVERRIDE THE GENERIC INVOICE ADD RESPONSE
		public function invoiceAddRs($requestID, $user, $action, $ID, $extra, $err, $last_action_time, $last_actionident_time, $xml, $idents){
			//...DO SOMETHING WITH THE $xml VARIABLE HERE
		}
		*/


		//.... ADD ADDITIONAL METHODS HERE		
	}